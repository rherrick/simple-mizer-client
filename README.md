# XNAT Simple Mizer Client #

To run the SimpleMizerClient as an application, you first need to build the application, then extract it and run it 
using the application script.

## Build the application ##

In the source folder, run the following command:

```
$ ./gradlew clean distZip
``` 

This builds the jar file for the code into the folder **build/libs**, but also builds a zip archive containing the
application jar, its run-time dependencies, and scripts for running the application. This archive can be found in the
folder **build/distributions**.

## Extracting the application ##

Extract the application to a folder somewhere. The following command will unzip the application into your home folder:

```
$ unzip build/distributions/SimpleMizerClient-1.0-SNAPSHOT.zip -d ~
```

Now add the **bin** folder of the extracted application to your PATH, e.g.:

```
$ export PATH=~/SimpleMizerClient-1.0-SNAPSHOT/bin:${PATH}
```

## Running the application ##

To run the application, you need a DICOM file to be anonymized along with an anonymization script. You can copy the
sample DICOM and test script from the project's unit test code:

```
$ cp src/test/resources/sample.dcm src/test/resources/test.das .
``` 

With the **bin** folder of the SimpleMizerClient on your path, you can run the client with the following command:

```
$ SimpleMizerClient --dicom sample.dcm --script test.das --output output.dcm
```

This applies the anonymization script in **sample.das** to the DICOM file **sample.dcm**, with the resulting
output in the file **output.dcm**. You can then dump the contents of your output file to verify that the application
properly anonymized your DICOM metadata. The test script contains the following operations:

```
version "6.2"
(0008,0020) := "20200815"
(0008,0021) := "20200815"
(0008,0022) := "20200815"
(0008,0023) := "20200815"
```  

The sample DICOM values for the tags specified in the test script are:

```
$ dcmdump +P 0008,0020 +P 0008,0021 +P 0008,0022 +P 0008,0023 sample.dcm                                                                                                      [master|✚2…]
(0008,0020) DA [20200112]                               #   8, 1 StudyDate
(0008,0021) DA [20200112]                               #   8, 1 SeriesDate
(0008,0022) DA [20200112]                               #   8, 1 AcquisitionDate
(0008,0023) DA [20200112]                               #   8, 1 ContentDate
```

The output DICOM should include those same headers, but with different values. Dumping that output yields:

```
$ dcmdump +P 0008,0020 +P 0008,0021 +P 0008,0022 +P 0008,0023 output.dcm                                                                                                      [master|✚2…]
(0008,0020) DA [20200815]                               #   8, 1 StudyDate
(0008,0021) DA [20200815]                               #   8, 1 SeriesDate
(0008,0022) DA [20200815]                               #   8, 1 AcquisitionDate
(0008,0023) DA [20200815]                               #   8, 1 ContentDate
``` 

DicomEdit 6.2 contains a lot more features than simply changing tag values. Documentation for the language is available
on the [XNAT documentation wiki](https://wiki.xnat.org/xnat-tools/dicomedit/dicomedit-6-2-language-reference).