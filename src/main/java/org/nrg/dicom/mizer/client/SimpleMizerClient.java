package org.nrg.dicom.mizer.client;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.nrg.dcm.edit.mizer.DE4Mizer;
import org.nrg.dicom.dicomedit.mizer.DE6Mizer;
import org.nrg.dicom.mizer.service.MizerService;
import org.nrg.dicom.mizer.service.impl.BaseMizerService;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.Callable;

@Slf4j
public class SimpleMizerClient implements Callable<File> {
    public SimpleMizerClient() {
        this(null, null, null);
    }

    public SimpleMizerClient(final String dicom, final String script, final String output) {
        _dicom = dicom;
        _script = script;
        _output = output;
    }

    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(final String[] arguments) throws Exception {
        final Options options = new Options();

        final Option dicomInput = new Option("d", "dicom", true, "DICOM input file");
        dicomInput.setRequired(true);
        options.addOption(dicomInput);
        final Option scriptInput = new Option("s", "script", true, "Anonymization script file");
        scriptInput.setRequired(true);
        options.addOption(scriptInput);
        final Option dicomOutput = new Option("o", "output", true, "DICOM output file");
        dicomOutput.setRequired(true);
        options.addOption(dicomOutput);

        final CommandLineParser parser    = new DefaultParser();
        final HelpFormatter     formatter = new HelpFormatter();

        try {
            final CommandLine commandLine = parser.parse(options, arguments);
            final String      dicom       = commandLine.getOptionValue("dicom");
            final String      script      = commandLine.getOptionValue("script");
            final String      output      = commandLine.getOptionValue("output");
            log.info("Preparing to run anonymization on file {} with the script {}. Output will be placed in {}", dicom, script, output);
            final SimpleMizerClient client = new SimpleMizerClient(dicom, script, output);
            client.call();
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        }
    }

    @Override
    public File call() throws Exception {
        final MizerService service    = new BaseMizerService(Arrays.asList(new DE4Mizer(), new DE6Mizer()));
        final File         outputFile = getOutputFile(_output);
        try (final FileInputStream dicom = new FileInputStream(getFile(_dicom));
             final FileOutputStream output = new FileOutputStream(outputFile)) {
            IOUtils.copy(dicom, output);
        }
        try (final FileInputStream script = new FileInputStream(getFile(_script))) {
            service.anonymize(outputFile, new MizerContextWithScript(script));
        }
        return outputFile;
    }

    private static File getOutputFile(final String path) throws FileAlreadyExistsException {
        final File file = Paths.get(path).toFile();
        if (file.exists()) {
            throw new FileAlreadyExistsException("A file already exists at the path " + path);
        }
        final File parent = ObjectUtils.defaultIfNull(file.getParentFile(), new File("."));
        if (parent.exists()) {
            if (!parent.isDirectory()) {
                throw new FileAlreadyExistsException("The specified parent for the output file " + path + " exists but is not a folder.");
            }
        } else {
            parent.mkdirs();
        }
        return file;
    }

    private static File getFile(final String path) throws FileNotFoundException {
        final File file = Paths.get(path).toFile();
        if (file.exists() && file.exists()) {
            return file;
        }
        throw new FileNotFoundException("The path " + path + " either doesn't exist or isn't a readable file.");
    }

    private final String _dicom;
    private final String _script;

    private final String _output;
}
