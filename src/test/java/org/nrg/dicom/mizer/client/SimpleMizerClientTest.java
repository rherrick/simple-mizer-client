package org.nrg.dicom.mizer.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

class SimpleMizerClientTest {
    @Test
    void appHasAGreeting() {
        final SimpleMizerClient client = new SimpleMizerClient();
        assertNotNull(client.getGreeting(), "app should have a greeting");
    }

    @Test
    void simpleAnonymization() throws Exception {
        final File dicom  = new File(getClass().getResource("/sample.dcm").toURI());
        final File script = new File(getClass().getResource("/test.das").toURI());
        final File folder = Files.createTempDirectory("simple-anon-test").toFile();
        final File output = folder.toPath().resolve("sample-anon.dcm").toFile();

        validate(dicom, "20200112");

        final SimpleMizerClient client = new SimpleMizerClient(dicom.getAbsolutePath(), script.getAbsolutePath(), output.getAbsolutePath());
        final File              result = client.call();

        validate(result, "20200815");
    }

    private void validate(final File dicom, final String date) throws IOException {
        try (final DicomInputStream original = new DicomInputStream(dicom)) {
            final DicomObject dicomObject     = original.readDicomObject();
            final String      studyDate       = dicomObject.getString(Tag.StudyDate);
            final String      seriesDate      = dicomObject.getString(Tag.SeriesDate);
            final String      acquisitionDate = dicomObject.getString(Tag.AcquisitionDate);
            final String      contentDate     = dicomObject.getString(Tag.ContentDate);
            assertThat(dicomObject).isNotNull();
            assertThat(studyDate).isEqualTo(date);
            assertThat(seriesDate).isEqualTo(date);
            assertThat(acquisitionDate).isEqualTo(date);
            assertThat(contentDate).isEqualTo(date);
        }
    }
}
